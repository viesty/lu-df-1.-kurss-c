/* *******************************************
Viesturs Ružāns, vr14008

B22. Atrast visus simetriskus skaitļus intervālā [n, m], kurus kāpinot kvadrātā iegūst simetrisku skaitli.
Skaitļu dalīšana ciparos jāveic skaitliski.

Programma izveidota: 12.09.2014
******************************************* */
#include <iostream>

using namespace std;
// Funkcija, kas nosaka, vai skaitlis ir simetrisks. Ja tā ir, tad tas atgriež šo pašu skaitli, ja nav, nulli.
int simetrija(int skaitlis){
    int kopija = skaitlis;
    int sum = 0;
    while(skaitlis!=0){
        int rem;
        rem = skaitlis%10; //iegūst pēdējo ciparu skaitlī
        sum = sum*10 + rem;
        skaitlis/=10; //skaitlim "nocērt" pēdējo ciparu
        if(sum == kopija){
            return sum;
        }
    }
    return 0;
}
int main()
{
    int cont;
    do{
        int a, b, counter = 0;
        cout << "Ievadi intervala sakuma skaitli: ";
        cin >> a;
        cout << "Ievadi intervala beigu skaitli: ";
        cin >> b;
        cout << "Intervals ir [" << a << ";" << b << "]\n";
        if(a>b){
            cout << "Nepareizs intervala formats! Intervals sakas no mazaka skaitla ejot uz lielaku.\n";
        }else{
            for(int i = a; i<=b; i++){
                if(i == 0){ //gadījums, kad intervāla vērtība ir 0, kas kvadrātā arī ir 0, kas ir simetrisks skaitlis
                    cout << i << " ir simetrisks skaitlis, kuru kapinot kvadrata iegast simetrisku skaitli.\n";
                    continue;
                }
                if (simetrija(i) == 0){ //ja skaitlis nav simetrisks, grūžam ciklā nākamo intervāla vērtību
                    continue;
                }
                int kvadrats = i*i;
                if(kvadrats==simetrija(kvadrats)){ //pārbaudam, vai intervāla vērtības kvadrāts ir simetrisks
                    cout<<i<<" ir simetrisks skaitlis, kuru kapinot kvadrata iegust simetrisku skaitli.\n";
                    counter++;
                }
            }
            if(counter == 0){
                cout << "Saja intervala nav simetrisku skaitlu, kurus kapinot kvadrata var iegut simetrisku skaitli.";
            }
        }
        cout << "\nVai velies turpinat (1) programmas izpildi, vai ari to partraukt (0) ?: ";
        cin >> cont;
    }while(cont == 1);
    return 0;
}
