#ifndef PROGRAM_H
#define PROGRAM_H
/*
D19. Izveidot klasi "Burtu mas�vs", kur� glab�jas mas�vs ar burtiem (char) garum� 10. Klasei izveidot ��das metodes:
(1) konstruktors, kas aizpilda mas�vu ar simboliem 'a' (lietot vienu papildus elementu ar indeksu 10, kur� ierakst�t simbolu virknes beigu simbolu '\0'),
(2) destruktors, kur� pazi�o par objekta likvid��anu,
(3) metode "Main�t" ar main�m� elementa indeksu (0..9) un jauno v�rt�bu, kas izmaina main�m� mas�va elementa v�rt�bu,
(4) metode "Lielie", kas p�rveido visus burtus par lielajiem,
(5) metode "Mazie", kas p�rveido visus burtus par mazajiem,
(6) metode "Druk�t", kas izdruk� mas�v� glab�to v�rdu.
*/

class burtuMasivs
{
    private:
        char burti[10]={};
        int change;
        char newValue;
    public:
        burtuMasivs();
        ~burtuMasivs();
        void metodeDrukat();
        void metodeLielie();
        void metodeMainit();
        void metodeMazie();
};

#endif // PROGRAM_H
