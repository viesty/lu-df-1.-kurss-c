#include "program.h"
#include <iostream>
using namespace std;
/*
D19. Izveidot klasi "Burtu mas�vs", kur� glab�jas mas�vs ar burtiem (char) garum� 10. Klasei izveidot ��das metodes:
(1) konstruktors, kas aizpilda mas�vu ar simboliem 'a' (lietot vienu papildus elementu ar indeksu 10, kur� ierakst�t simbolu virknes beigu simbolu '\0'),
(2) destruktors, kur� pazi�o par objekta likvid��anu,
(3) metode "Main�t" ar main�m� elementa indeksu (0..9) un jauno v�rt�bu, kas izmaina main�m� mas�va elementa v�rt�bu,
(4) metode "Lielie", kas p�rveido visus burtus par lielajiem,
(5) metode "Mazie", kas p�rveido visus burtus par mazajiem,
(6) metode "Druk�t", kas izdruk� mas�v� glab�to v�rdu.
*/
burtuMasivs::burtuMasivs()
{
    fill(burti,burti+10,'a'); //aizpilda masivu burti ar burtiem a, beigas pieliekot beigu simbolu
    fill(burti+10,burti+11,'\0');
}

burtuMasivs::~burtuMasivs()
{
    cout << "Burtu masivs likvidets" << endl;
}
void burtuMasivs::metodeMainit()
{
    //metode "Main�t" ar main�m� elementa indeksu (0..9) un jauno v�rt�bu, kas izmaina main�m� mas�va elementa v�rt�bu,
    cout << "Elements, kuru mainit: ";
    cin >> change;
    if(change>-1 && change <10){
        cout << "Jauna elementa vertiba: ";
        cin >> newValue;
        burti[change] = newValue;
    }else{
        cout << "Nepareiza vertiba! Ir iespejams mainit tikai elementus no 0. lidz 9. indeksam" << endl;
        metodeMainit();
    }
}
void burtuMasivs::metodeLielie()
{
    //metode "Lielie", kas p�rveido visus burtus par lielajiem,
    for(int i=0; i<10; i++){
        //ja decimalais kods atbilst visiem mazo burtu kodiem, tad tos pamaina par -32, kas atbilst lielo burtu kodam
        if((int)burti[i] > 96 && (int)burti[i] < 123){
            //decimalo kodu parvers atpakal par char
            burti[i]=(char)((int)burti[i])-32;
        }
    }
}
void burtuMasivs::metodeMazie()
{
    //metode "Mazie", kas p�rveido visus burtus par mazajiem,
    for(int i=0; i<10; i++){
        //tas pats, kas ar mazajiem, bet pieskaitot 32
        if((int)burti[i] > 64 && (int)burti[i] < 91){
            burti[i]=(char)((int)burti[i])+32;
        }
    }
}
void burtuMasivs::metodeDrukat()
{
    //metode "Druk�t", kas izdruk� mas�v� glab�to v�rdu.
    cout << "Burtu masivs: ";
    for(int i=0; i<10; i++){
        cout << burti[i];
    }
    cout << endl;
}
