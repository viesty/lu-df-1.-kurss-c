/*
Izveidot programmu valod� C++, izmantojot objektorient�t�s programm��anas l�dzek�us un struktur�jot programmu vismaz tr�s failos.
Klases hederi oblig�ti novietot atsevi��� hedera fail� (program.h). Visas metodes realiz�t �rpus hedera faila � speci�l� C++ fail� (program.cpp).
Funkcija main ievietojama v�l cit� C++ fail� (main.cpp).
Klases dati p�c noklus��anas ir sl�pti (private), bet metodes atkl�tas (public).
��s programmas ietvaros NAV oblig�ti j�nodro�ina t�da lietot�ja saskarne, k�da tika pras�ta iepriek��jos tr�s uzdevumos (main funkciju dr�kst �cieti� iekod�t).
Citas pras�bas sk. Laboratorijas darbu noteikumos.

D19. Izveidot klasi "Burtu mas�vs", kur� glab�jas mas�vs ar burtiem (char) garum� 10. Klasei izveidot ��das metodes:
(1) konstruktors, kas aizpilda mas�vu ar simboliem 'a' (lietot vienu papildus elementu ar indeksu 10, kur� ierakst�t simbolu virknes beigu simbolu '\0'),
(2) destruktors, kur� pazi�o par objekta likvid��anu,
(3) metode "Main�t" ar main�m� elementa indeksu (0..9) un jauno v�rt�bu, kas izmaina main�m� mas�va elementa v�rt�bu,
(4) metode "Lielie", kas p�rveido visus burtus par lielajiem,
(5) metode "Mazie", kas p�rveido visus burtus par mazajiem,
(6) metode "Druk�t", kas izdruk� mas�v� glab�to v�rdu.
*/
#include <iostream>
#include "program.h"
using namespace std;

int main()
{
    burtuMasivs b;
    int i;

    cout << "===================================="<< endl;
    cout << "Izvelies, ko velies darit:"<<endl;
    cout << "[1] Metode \"Mainit\", kas nomaina izveletu masiva elementu." << endl;
    cout << "[2] Metode \"Lielie\", kas parveido visus burtus par lielajiem." << endl;
    cout << "[3] Metode \"Mazie\", kas parveido visus burtus par mazajiem." << endl;
    cout << "[4] Metode \"Drukat\", kas izdruka masiva glabato vardu." << endl;
    cout << "Jebkurs cits taustins - partraukt darbu" << endl;
    do{
        cout << "===================================="<< endl;
        cout << "Tava izvele: ";
        cin >> i;
        //Izveles noteiksanas switch's
        switch(i){
        case 1:
            b.metodeMainit();
            break;

        case 2:
            b.metodeLielie();
            break;

        case 3:
            b.metodeMazie();
            break;

        case 4:
            b.metodeDrukat();
            break;

        default:
            break;
        }
    }while(i>0 && i<5);
    return 0;
}
