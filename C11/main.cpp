﻿/* *******************************************
Viesturs Ružāns, vr14008

C11. Dotas divas (zema limena) simbolu virknes  a un b (sastav no cipariem un neobligati minusa zimes),
kas reprezente divus veselus skaitlus. Izveidot funkciju, kas izveido treso simbolu virkni c,
kas reprezente skaitli un ir simbolu virknu a un b reprezenteto skaitlu summa.
Saskaitisana vai atnemsana javeic pa vienam ciparam (ka to dara uz papira).
Funkcijai jaspej apstradat simbolu virknes lidz garumam 100 ieskaitot.

Programma izveidota: 03.10.2014
******************************************* */
#include <iostream>
#include <cstring>
#include <algorithm>
#include <string>

using namespace std;
bool negativs, negativs1, negativs2, negativs_rezultats;
string rez;
int counter;

char atnemsana(int sk1garums,int sk2garums,char skaitlis1[101],char skaitlis2[101], char c[102]){

    //noskaidrojam, kurš ir lielākais skaitlis - int variants neder, jo skaitlis var būt pat 100 simbolus garš

    int q = 0; //apzīmēs lielākā skaitļa garumu
    int w = 0; //apzīmēs mazākā skaitļa garumu
    int e = 0; //e ir tikai palīgmainīgais, lai saglabātu kādu citu vērtību maiņas gadījumā
    char skaitlis_tmp[101] = {}; //palīgmainīgais, lai saglabātu citu char masīvu maiņas gadījumā.
    negativs_rezultats = false;
    bool nextSubtract = false;
    rez = "";
    counter = 0;
    //ja skaitlis1 ir garāks, tad tas loģiski ir arī lielāks. Tā pat arī otrādi.
    if(sk1garums > sk2garums){
        q = sk1garums;
        w = sk2garums;
        if(negativs1){
            //ja 1. skaitlis bija negatīvs un pie tam lielāks, tad rezultāts arī būs negatīvs
            negativs_rezultats = true;
        }
    }else if(sk2garums > sk1garums){
        q = sk2garums;
        w = sk1garums;
        //samainam skaitlis1 un skaitlis2 vērtības, lai tās atbilst a un b vērtībām (lielākais skaitlis augšā, mazākais apakšā)
        strcpy(skaitlis_tmp, skaitlis1);
        strcpy(skaitlis1, skaitlis2);
        strcpy(skaitlis2, skaitlis_tmp);
        if(negativs2){
            //ja 2. skaitlis bija negatīvs un pie tam lielāks, tad rezultāts arī būs negatīvs
            negativs_rezultats = true;
        }
    }else{
        for(int i=0; i<sk1garums; i++){
            //ja abi skaitļi ir vienāda garuma, nākas pārbaudīt katru ciparu, sākot ar nullto indeksu (kolīdz viens cipars ir lielāks nekā otrs,
            //konkrētais skaitlis arī ir lielāks.
            if(skaitlis1[i] > skaitlis2[i]){
                q = sk1garums;
                w = sk2garums;
                if(negativs1){
                    negativs_rezultats = true;
                }
                break;
            }else if(skaitlis2[i] > skaitlis1[i]){
                q = sk2garums;
                w = sk1garums;
                strcpy(skaitlis_tmp, skaitlis1);
                strcpy(skaitlis1, skaitlis2);
                strcpy(skaitlis2, skaitlis_tmp);
                if(negativs2){
                    negativs_rezultats = true;
                }
                break;
            }else{
                continue;
            }
        }
    }
    if(q == 0 && w == 0){
        //ja abi skaitļi ir identiski, tad rezultātu var atgriezt uzreiz, tas būs 0
        c[counter] = '0';
        counter++;
        return c[102];
    }
    //lielako liekam augšā, mazāko apaksa, tātad, no lielākā atņemam mazāko
    for(q; q>0; q--){
        /*
            int sk1 = skaitlis1[a-1]
            parbaudam vai ir izdaritas visas darbibas ar B:
                vai nextSubtract == true:
                    ja ja:
                        ja a == 1:
                            atnemam 1 no sk1
                            ja veidojas 0:
                                beidzam programmu, jo tas bija pedejais skaitlis un tika atnemts, piem 100-10 = 0|90
                            ja neveidojas 0:
                                ta pat beidzam :D
                        ja a > 1:
                            ja sk1 < 1:
                                sk1+10;
                                atnemam 1 no sk1
                                nextSubtract = true
                            ja sk1 > 0:
                                atnemam 1 no sk1
                                nextSubtract = false
                    ja ne:
                        vnk norakstam visus sk1 kas bus
        */
        if(w<1){
            while(q>0){
                //iegūstam ciparu
                int sk1 = (int)skaitlis1[q-1]-'0';
                if(nextSubtract){
                    //vai esam "aizņēmušies" iepriekšējā darbībā?
                    if(q == 1){
                        //vai šis ir pēdējais (reāli pirmais) cipars
                        sk1--;
                        if(sk1<1){
                            //ja atņemot no pēdējā (reāli pirmā) cipars ieguvām 0, tad 0 nesaglabājam (jo priekšā 0 nav vajadzīga)
                            if(negativs_rezultats){
                                //ja rezultātam ir jābūt negatīvam, piekabinam nulli beigās
                                c[counter] = '-';
                                counter++;
                            }
                            return c[102];
                        }
                        c[counter] = (char)sk1+'0';
                        counter++;
                        if(negativs_rezultats){
                                c[counter] = '-';
                                counter++;
                        }
                        return c[102];
                    }
                    else if(q > 1){
                        if(sk1<1){
                            //vai "jāaizņemās" no nākamā cipara?
                            sk1+=10;
                            sk1--;
                            nextSubtract = true;
                            c[counter] = sk1+'0';
                            counter++;
                        }
                        else if(sk1 > 0){
                            sk1--;
                            nextSubtract = false;
                            c[counter] = (char)sk1+'0';
                            counter++;
                        }
                    }
                }else{
                    //ja nav jāveic manipulācija ar atlikušajiem skaitļiem, vienkārši tos norakstam
                    c[counter] = sk1+'0';
                    counter++;
                    if(q == 1 && negativs_rezultats){
                        c[counter] = '-';
                        counter++;
                    }
                }

                break;
            }
        }

        while(w>0){
            /*
                No skaitlis1[a-1] atnemam skaitlis2[b-1]:
                    vai skaitlis1[a-1] ir mazaks neka skaitlis2[b-1]?:
                        ja ja:
                            tad skaitlis1[a-1] + 10.
                            no nakama skaitlis1[a-1] jaatnem 1, jo aiznemamies
                        ja ne:
                            tad skaitlis1[a-1] - skaitlis2[b-1]
            */
            //sk1 ir pēdējais pirmā skaitļa cipars, sk2 pēdējais otrā skaitļa cipars
            int sk1 = (int)skaitlis1[q-1]-'0';
            int sk2 = (int)skaitlis2[w-1]-'0';
            if(nextSubtract == true){
                //ja mēs aizņēmāmies no sk1-1 cipara, tad ir jāatņem 1
                sk1--;
                nextSubtract = false;
            }
            if(sk1 < sk2){
                //ja pirmais cipars ir mazāks nekā otrais, atņemt nav iespējams, tāpēc jāpieskaita 10, aizņemoties no sk1-1 cipara
                sk1+=10;
                nextSubtract = true;
            }
            int starpiba = sk1-sk2;
            c[counter] = (char)starpiba+'0';
            counter++;
            //ja esam sasnieguši pēdējo ciparu abās skaitļu virknēs un skaitlim jābūt negatīvam, piekabinam mīnusu
            if(negativs_rezultats && q == 1 && w == 1){
                //rez+='-';
                c[counter] = '-';
                counter++;
            }
            w--;
            break;
        }
    }
    return c[102];
}
int saskaitisana(int q, int w, char skaitlis1[101], char skaitlis2[101], char c[102]){
    int betweensum;
    bool nextIterPlus = false;
    rez = "";
    counter = 0;
    for(q; q > 0; q--){
        if(w==0){
            while(q>0){
                //iegūst apskatāmo augšējās rindas ciparu
                betweensum = (int)skaitlis1[q-1]-'0';
                if(nextIterPlus == true){
                    betweensum++;
                    nextIterPlus = false;
                }
                if(betweensum > 9 && w == 0 && q == 1){
                    //ja šis ir pēdējais cipars augšējā skaitļa rindā, tad rezultāta priekšā pielieikam 1, aiz tā liekam atlikumu no summa/10
                    c[counter] = (char)betweensum%10+'0';
                    counter++;
                    c[counter] = (char)1+'0';
                    counter++;
                    w--;
                    break;
                }
                if(betweensum > 9){
                    //ja cipars ir lielāks par 9, tad nākamajā iterācijā pieskaitīsim 1, bet te atstāsim tikai atlikumu ciparam dalīts ar 10
                    betweensum%=10;
                    nextIterPlus = true;
                }
                c[counter] = (char)betweensum+'0';
                counter++;
                q--;
            }
        }
        while (w > 0){
            //sarēķinam summu abiem [a-1] un [b-1] cipariem
            betweensum = ((int)skaitlis1[q-1]-'0') + ((int)skaitlis2[w-1]-'0');
            if(nextIterPlus == true){
                //ja summa bija pāri 9 un nākamajam ciparam jāpieskaita 1
                betweensum++;
                nextIterPlus = false;
            }
            if(betweensum > 9 && w == 1 && q == 1){
                //ja šis ir pēdējais cipars abās skaitļu rindās, tad priekšā pielieikam 1, aiz tā liekam atlikumu no summa/10
               c[counter] = (char)betweensum%10+'0';
               counter++;
               c[counter] = (char)1+'0';
               counter++;
               w--;
               break;
            }
            if(betweensum > 9){
                //nosaka, vai nākamajā iterācijā jāpieskaita ciparam 1
                betweensum%=10;
                nextIterPlus = true;
            }
            c[counter] = (char)betweensum+'0';
            counter++;
            w--;
            break;
        }
    }
    if(negativs == true && (skaitlis1[0]!='0' && skaitlis2[0]!='0')){
        //ja rezultātam jābūt negatīvam, piekabina mīnusu
        negativs_rezultats = true;
        c[counter]='-';
        counter++;
    }
    return c[102];
}

int main()
{
    int cont = 1;
    do{
    rez = "";
    negativs = false;
    negativs1 = false;
    negativs2 = false;
    negativs_rezultats = false;
    char a[101];
    char b[101];
    char skaitlis1tmp[101]; //palīgmainīgais vecās vērtības saglabāšanai
    char skaitlis2tmp[101]; //palīgmainīgais vecās vērtības saglabāšanai
    char c[102] = {};
    cout << "Ievadi pirmo skaitli (veselu): ";
    cin.getline(a, 101); //iegūst rindu ar cipariem, ko iegrūž a masīvā
    cin.clear();
    cin.ignore(102,'\n'); //nepieciešams, lai \n simbolu un nebūtu problēmu ar atkārtotu ciparu rindas ievadi
    cout << "Ievadi otro skaitli (veselu): ";
    cin.getline(b, 101); //iegūst rindu ar cipariem, ko iegrūž b masīvā

    //ja atrod mīnusa zīmi, to izvāc, taču saglabā "negativs" flagu
    if(a[0] == '-' && b[0] == '-'){
        strncpy(skaitlis1tmp, a+1, 100);
        strncpy(skaitlis2tmp, b+1, 100);
        strcpy(a, skaitlis1tmp);
        strcpy(b, skaitlis2tmp);
        negativs = true;
    }else if (a[0] == '-'){
        strncpy(skaitlis1tmp, a+1, 100);
        strcpy(a, skaitlis1tmp);
        negativs1 = true;
    }
    else if (b[0] == '-'){
        strncpy(skaitlis2tmp, b+1, 100);
        strcpy(b, skaitlis2tmp);
        negativs2 = true;
    }

    int sk1garums = strlen(a);
    int sk2garums = strlen(b);
    /*
    nosaka, kurš skaitlis ir garāks (rēķināšanā uz papīra garāko raksta augšā, īsāko apakšā)
    */
    if(negativs1 == true || negativs2 == true){
        atnemsana(sk1garums, sk2garums, a, b, c);
    }else if(negativs == true){
        saskaitisana(sk1garums, sk2garums, a, b, c);
    }else if(sk1garums>sk2garums || sk1garums == sk2garums){
        saskaitisana(sk1garums, sk2garums, a, b, c);
    }else if (sk1garums<sk2garums){
        saskaitisana(sk2garums, sk1garums, b, a, c);
    }
    //Apgriežam masīvu c
    char c_tmp[102] = {};
    for(int x = 0; x < counter; x++){
        for(int y = counter-1; y >= 0; y--){
            //if(negativs_rezultats == false && c[y] == '0') continue;
            c_tmp[y] = c[x];
            x++;
            continue;
        }
    }

    //noņem nulles no priekšas, piemēram, 444-446 būtu -002 un 555-550 = 005
    if(c_tmp[0] == '-' && c_tmp[1] == '0'){
        //nodzēš esošo c masīvu
        for(int i = 0; i < counter; i++){
            c[i] = '\0';
        }
        c[0] = '-';
        int idCounter = 1;
        bool irSkaitlis = false;
        for(int i = 1; i < counter; i++){
            if(c_tmp[i] == '0' && irSkaitlis == false){
                continue;
            }else{
                irSkaitlis = true;
                c[idCounter] = c_tmp[i];
                idCounter++;
            }
        }
    }
    else if(counter > 1 && c_tmp[0] == '0'){
        //nodzēš esošo c masīvu
        for(int i = 0; i < counter; i++){
            c[i] = '\0';
        }
        int idCounter = 0;
        bool irSkaitlis = false;
        for(int i = 0; i < counter; i++){
            if(c_tmp[i] == '0' && irSkaitlis == false){
                continue;
            }else{
                irSkaitlis = true;
                c[idCounter] = c_tmp[i];
                idCounter++;
            }
        }
    }
    else{
        strcpy(c, c_tmp);
    }
    cout << "Rezultats ir: " << c;

    cout << endl << "Turpinat (1) vai beigt(0)?: ";
    cin >> cont;
    cin.ignore();
    }while(cont == 1);
    return 0;
}
