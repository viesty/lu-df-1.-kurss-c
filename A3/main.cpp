/* *******************************************

Viesturs Ružāns, vr14008

A3. Dots naturāls skaitlis n. Noskaidrot, vai šī skaitļa pirmais cipars ir vienāds ar pēdējo ciparu (piemēram, 18961).
Skaitļa dalīšana ciparos jāveic skaitliski.

Programma izveidota: 08.09.2014

******************************************* */
#include <iostream>

using namespace std;

int determine(){
    int n;
    cout << "Ievadi naturalu skaitli n: ";
    cin >> n;
    if(n<=0){
        cout << n <<" nav naturals skaitlis!";
        return 0;
    }
    else if (n/10 == 0){ //nosaka vai skaitlis ir viencipara
        cout << n <<" ir viencipara skaitlis, tapec tam nevar pirmais cipars but vienads ar pedejo!";
        return 0;
    }
    int pedejais = n % 10; //iegūst pēdējo ciparu no skaitļa n, ar ko arī jāsalīdzina pirmais
    int pirmais;
    while(n>0){ //Katrā cikla iterācijā norādītais skaitlis n tiek izdalīts ar 10, lai no labās puses "nocirstu" pēdējo skaitli,
                //kamēr paliek tikai viens cipars (pirmais)
        pirmais = n % 10;
        n /= 10;
    }
    if(pirmais == pedejais){
        cout << "Ievadita skaitla pirmais un pedejais cipars ir vienadi!";
    }
    else{
        cout << "Ievadita skaitla pirmais un pedejais cipars nav vienadi!";
    }
    return 0;
}
int main()
{
    int cont;
    do{ //atkārto programmas darbību, kamēr vien lietotājs izvēlas to turpināt.
        determine();
        cout << "\nVai velies sakt no sakuma (1) vai beigt (0) programmas lietosanu? : ";
        cin >> cont;
    } while (cont == 1);
    return 0;
}
/*
+--------+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------+
| Ievads |                       Programmas vēlamā reakcija                        |                                              Rezultāts                                              |
+--------+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------+
|      4 | Kļūda. Viencipara skaitlī nav pirmā un pēdējā cipara                    | Izdod paziņojumu: “4 ir viencipara skaitlis, tapec tam nevar pirmais cipars but vienads ar pedejo!” |
|   4566 | Paziņojums, ka ievadītā skaitļa pirmais un pēdējais cipari nav vienādi. | +                                                                                                   |
|  86478 | Paziņojums, ka ievadītā skaitļa pirmais un pēdējais cipari ir vienādi.  | +                                                                                                   |
|   -343 | Kļūda. Negatīvs skaitlis nav naturāls skaitlis.                         | Izdod paziņojumu: “-343 nav naturals skaitlis!”                                                     |
|      0 | Kļūda. Nulle nav naturāls skaitlis.                                     | Izdod paziņojumu: “0 nav naturals skaitlis!”                                                        |
+--------+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------+
*/
